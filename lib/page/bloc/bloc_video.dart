import 'dart:io';
import 'package:video_player/video_player.dart';
import 'package:flutter/material.dart';
import 'package:camera/camera.dart';
import 'package:rxdart/rxdart.dart';
import 'dart:async';
import 'package:path_provider/path_provider.dart';

class BlocVideo {
  var cameras = BehaviorSubject<List<CameraDescription>>();
  var selectCamera = BehaviorSubject<bool>();
  var videoPath = BehaviorSubject<File>();
  var cameraOn = BehaviorSubject<int>();
  var videoOn = BehaviorSubject<bool>();
  var playPause = BehaviorSubject<bool>.seeded(false);
  var timeVideo = BehaviorSubject<double>.seeded(0.0);
  FloatingActionButtonLocation fabLocation =
      FloatingActionButtonLocation.centerDocked;

  CameraController controlCamera;
  VideoPlayerController controlVideo;

  Future getCameras() async {
    await availableCameras().then((lista) {
      cameras.sink.add(lista);
    }).catchError((e) {
      print("ERROR CAMERA: $e");
    });
  }

  Future<String> takePicture() async {
    if (!controlCamera.value.isInitialized) {
      print("selecionado camera");
      return null;
    }
    final Directory extDir = await getApplicationDocumentsDirectory();
    final String dirPath = '${extDir.path}/Pictures/flutter_test';
    await Directory(dirPath).create(recursive: true);
    final String filePath = '$dirPath/${timestamp()}.jpg';

    if (controlCamera.value.isTakingPicture) {
      // A capture is already pending, do nothing.
      return null;
    }

    try {
      await controlCamera.takePicture();
    } on CameraException catch (e) {
      print(e);
      return null;
    }
    return filePath;
  }

  String timestamp() => DateTime.now().millisecondsSinceEpoch.toString();

  void onNewCameraSelected(CameraDescription cameraDescription) async {
    selectCamera.sink.add(null);
    if (controlCamera != null) {
      await controlCamera.dispose();
    }
    controlCamera =
        CameraController(cameraDescription, ResolutionPreset.medium);
    controlCamera.addListener(() {
      if (controlCamera.value.hasError) selectCamera.sink.add(false);
    });

    await controlCamera.initialize().then((value) {
      selectCamera.sink.add(true);
    }).catchError((e) {
      print(e);
    });
  }

  void changeCamera() {
    var list = cameras.value;

    if (list.length == 2) {
      if (controlCamera.description.name == "0") {
        onNewCameraSelected(list[1]);
        cameraOn.sink.add(1);
      } else {
        onNewCameraSelected(list[0]);
        cameraOn.sink.add(0);
      }
    }
  }

  void deleteVideo() {
    var dir = new Directory(videoPath.value.path);
    dir.deleteSync(recursive: true);
    videoPath.sink.add(null);
    videoOn.sink.add(null);
  }

  //Recording video

  void onVideoRecordButtonPressed() {
    fabLocation = FloatingActionButtonLocation.centerFloat;
    startVideoRecording().then((String filePath) {
      if (filePath != null) videoPath.sink.add(File(filePath));
    });
  }

  void onStopButtonPressed() {
    fabLocation = FloatingActionButtonLocation.centerDocked;
    stopVideoRecording().then((_) {
      //AQUI VC PAUSA O VIDEO
    });
  }

  Future<String> startVideoRecording() async {
    final Directory extDir = await getApplicationDocumentsDirectory();
    final String dirPath = '${extDir.path}/Movies/flutter_test';
    await Directory(dirPath).create(recursive: true);
    final String filePath = '$dirPath/${timestamp()}.mp4';

    if (controlCamera.value.isRecordingVideo) {
      // A recording is already started, do nothing.
      return null;
    }

    try {
      await controlCamera.startVideoRecording();
    } on CameraException catch (e) {
      print(e);
      return null;
    }
    videoOn.sink.add(false);
    /*  Timer.periodic(Duration(seconds: 1), (time) {
      var value = timeVideo.value * 60;
      if (time.tick == 63 || videoOn.value == true) {
        onStopButtonPressed();
        timeVideo.sink.add(0);
        time.cancel();
      } else {
        value++;
        print("valor: ${value / 60}");
        timeVideo.sink.add(value / 60);
      }
    });*/

    return filePath;
  }

  Future<void> stopVideoRecording() async {
    if (!controlCamera.value.isRecordingVideo) {
      return null;
    }

    try {
      await controlCamera.stopVideoRecording();
    } on CameraException catch (e) {
      print(e);
      return null;
    }

    _startVideoPlayer();
    if (controlVideo != null) videoOn.sink.add(true);
  }

  Future<void> _startVideoPlayer() async {
    controlVideo = VideoPlayerController.file(videoPath.value);

    await controlVideo.initialize();
    await controlVideo.setLooping(true);
    await controlVideo.play();
    playPause.sink.add(true);
  }

  void dispose() {
    cameras.close();
    controlCamera.dispose();
    controlVideo.dispose();
    selectCamera.close();
    videoPath.close();
    cameraOn.close();
    videoOn.close();
    timeVideo.close();
    playPause.close();
  }
}
